import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfcreaComponent } from './pdfcrea.component';

describe('PdfcreaComponent', () => {
  let component: PdfcreaComponent;
  let fixture: ComponentFixture<PdfcreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdfcreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfcreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
