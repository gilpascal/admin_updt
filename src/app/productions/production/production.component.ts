import { Component, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import { PDFDocument, PDFPage } from 'pdf-lib';
import jsPDF from 'jspdf';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.scss']
})
export class ProductionComponent implements OnInit {

  constructor(private monhttp:HttpService, ) {}
  orders:any=[]
  pendingorders:any=[]
  okorders:any=[]
  cancelorders:any=[]
  tab:any=[];
  cpt=0
  modal='#'
  cacher=false;
  prod_face1:any;
  prod_face2:any;
  prod_face3:any;
  prod_face4:any
  pdf1:any;
  pdf2:any;
  pdf3:any;
  pdf4:any
  crea_print_items:any=[]
  crea_okprint_items:any=[]
  crea_acpprint_items:any=[]
  crea_cancelprint_items:any=[]

  crea_pack_items:any=[]
  crea_okpack_items:any=[]
  crea_acppack_items:any=[]
  crea_cancelpack_items:any=[]

  crea_gadget_items:any=[]
  crea_okgadget_items:any=[]
  crea_acpgadget_items:any=[]
  crea_cancelgadget_items:any=[]

  crea_cloth_items:any=[]
  crea_cancelcloth_items:any=[]
  crea_okcloth_items:any=[]
  crea_acpcloth_items:any=[]

  crea_disp_items:any=[]
  crea_okdisp_items:any=[]
  crea_acpdisp_items:any=[]
  crea_canceldisp_items:any=[]

  editor_items:any=[]
  acpeditor_items:any=[]
  okeditor_items:any=[]
  canceleditor_items:any=[]
  ordValue:any;
  ngOnInit(): void {
   
    this.monhttp.Aladin_orders().subscribe(
      res=>{
        this.orders=res.data
        console.log(this.orders)
      for(let item of this.orders){
        //console.log(item)
       //for(let i =0;i<this.orders.length;i++){
       // this.orders[i].description= JSON.parse(this.orders[i].description);
      //   
      //  }
        item.description= JSON.parse(item.description)
        if(item.description.type_product=="crea" && item.description.category=="vetement"){
          this.crea_cloth_items.push(item)
          
          
         }
        if(item.description.type_product=="crea" && item.description.category=="emballage"){
          this.crea_pack_items.push(item)
          

         

        }
        if(item.description.type_product=="editor" ){
          this.editor_items.push(item)
        }
    if(item.description.type_product=="crea" && item.description.category=="imprimer"){
      item.crea_print_items.push(item)
    }
    //if(item.description.type_product=="crea" && item.description.category=="affichage"){
    //  item.crea_disp_items.push(item)
    // 
    //}
    if(item.description.type_product=="crea" && item.description.category=="gadget"){
      item.crea_gadget_items.push(item)
      console.log(item)
    }
    
  
        }
        
  },
      err=>{
        console.log(err)
      }

    )
    this.monhttp.Getpending().subscribe(
      res=>{
        this.pendingorders=res.data
        
        //console.log( this.pendingorders)
      for( let items of this.pendingorders){

        items.description= JSON.parse(items.description)
         if(items.description.type_product=="crea" && items.description.category=="vetement"){
          this.crea_acpcloth_items.push(items)
          
         }
         if(items.description.type_product=="crea" && items.description.category=="emballage"){
          this.crea_acppack_items.push(items)
          
         }
         if(items.description.type_product=="crea" && items.description.category=="imprimer"){
          this.crea_acpprint_items.push(items)
          
         }
         if(items.description.type_product=="crea" && items.description.category=="affichage"){
          this.crea_acpdisp_items.push(items)
          
         }
         if(items.description.type_product=="crea" && items.description.category=="gadget"){
          this.crea_acpgadget_items.push(items)
          
         }
        
        if(items.description.type_product=="editor" ){
          this.acpeditor_items.push(items)
        }
       
        }
        
      },
      err=>{
        console.log(err);
      }

    )
    
  this.monhttp.GetordersLivre().subscribe(
    res=>{
      this.okorders=res.data
      //console.log(this.okorders)
    for(let items of this.okorders){
      
      items.description= JSON.parse(items.description)
       if(items.description.type_product=="crea" && items.description.category=="vetement"){
        this.crea_okcloth_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="emballage"){
        this.crea_okpack_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="imprimer"){
        this.crea_okprint_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="affichage"){
        this.crea_okdisp_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="gadget"){
        this.crea_okgadget_items.push(items)
        
       }
      if(items.description.type_product=="editor" ){
        this.okeditor_items.push(items)
      }

    }
    //console.log(this.orders)
    }
    ,
      err=>{
        console.log(err);
      }
  )
  this.monhttp.getCancel().subscribe(
    res=>{
     this.cancelorders=res.data
     console.log(this.cancelorders)
     for( let items of this.cancelorders){
      items.description= JSON.parse(items.description)
     if(items.description.type_product=="crea" && items.description.category=="vetement"){
      this.crea_cancelcloth_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="emballage"){
      this.crea_cancelpack_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="imprimer"){
      this.crea_cancelprint_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="affichage"){
      this.crea_canceldisp_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="gadget"){
      this.crea_cancelgadget_items.push(items)
      
     }
    if(items.description.type_product=="editor" ){
      this.canceleditor_items.push(items)
    }
  }
    },
    err=>{
      console.log(err)
    }
  )
  }
  
  toggel(){
    this.cacher=true
  }
    updateStatus(event:any){
    let id = event.target.id
    if(id!=undefined){
      
      let data={id:id,status:'pending'}
      this.monhttp.Updatestatus(data).subscribe(
        res=>{
          console.log(res)
          if(res.status==true){
            this.monhttp.Getpending().subscribe(
              res=>{
                //console.log(res)
                this.pendingorders=res.data
                console.log( this.pendingorders)
              }
            )
          }
        },
        err=>{
          console.log(err)
        }
      )
    }
    
    }
    updateok(event:any){
      let id= event.target.id
      if(id!=undefined){
        let data={id:id, status:'ok'}
        this.monhttp.updateok2(data).subscribe(
          res=>{
            if(res.status==true){
              this.monhttp.GetordersLivre().subscribe(
                res=>{
                  //console.log(res)
                  this.okorders=res.data
                  console.log(this.okorders)
                }
              )
            }
          },
          err=>{
            console.log(err)
          }
        )
      }
    }
    updatecancel(event:any){
      let id= event.target.id
      if(id!=undefined){
        let data={id:id, status:'rejected'}
        this.monhttp.Updatecancel(data).subscribe(
          res=>{
            if(res.status==true){
              this.monhttp.getCancel().subscribe(
                res=>{
                  //console.log(res)
                  this.cancelorders=res.data
                  console.log(this.cancelorders)
                }
              )
            }
          },
          err=>{
            console.log(err)
          }
        )
      }
    }

download(){
  let download=document.getElementById("imdone");
  this.monhttp.triggerMouse(download);
}


Zoomimg(event:any){
    
      if(this.cacher){
        let id=event.target.id;
        let src1=this.orders[+id].description.face1;
        let src2=this.orders[+id].description.face2;
        let src3=this.orders[+id].description.f3;
        let src4=this.orders[+id].description.f4;
        this.pdf1=src1;
        this.pdf2=src2;
        this.prod_face1=this.orders[+id].description.production_f1;
        this.prod_face2=this.orders[+id].description.production_f2;
        this.prod_face3=this.orders[+id].description.f3;
        this.prod_face4=this.orders[+id].description.f4;

        document.getElementById('img1')?.setAttribute('src',src1);
        document.getElementById('img2')?.setAttribute('src',src2);
        document.getElementById('img3')?.setAttribute('src',src3);
        document.getElementById('img4')?.setAttribute('src',src4);
        if(this.cpt>0){
          let modal=document.getElementById('modalbtn');
          this.monhttp.triggerMouse(modal);
        }else{
          this.cpt=this.cpt+1;
        }
        console.log(id);
      }else{
        this.toggel();
        let id=event.target.id;
        let src1=this.orders[+id].description.face1;
        let src2=this.orders[+id].description.face2;
        let src3=this.orders[+id].description.f3;
        let src4=this.orders[+id].description.f4;
        document.getElementById('img1')?.setAttribute('src',src1);
        document.getElementById('img2')?.setAttribute('src',src2);
        document.getElementById('img3')?.setAttribute('src',src3);
        document.getElementById('img4')?.setAttribute('src',src4);
        if(this.cpt>0){
          let modal=document.getElementById('modalbtn');
          this.monhttp.triggerMouse(modal);
        }else{
          this.cpt=this.cpt+1;
        }
  
      }
     

    }

 

    getOrderInfo(event:any){
      this.ordValue=event
      if(this.ordValue.f3 && this.ordValue.f3){
        this.copyPages() 
      }else{
        if(this.ordValue.f3){
          let pdf = document.getElementById("create");
  
          pdf?.setAttribute('href',this.ordValue.f3);
            this.monhttp.triggerMouse(pdf);
          
        }
      }

      //this.copyPages()
     console.log(this.copyPages())
    }
    async  copyPages() {
      const url1 = this.ordValue.f3

      const url2 = this.ordValue.f3
    
      const firstDonorPdfBytes = await fetch(url1).then(res => res.arrayBuffer())
      const secondDonorPdfBytes = await fetch(url2).then(res => res.arrayBuffer())
    
      const firstDonorPdfDoc = await PDFDocument.load(firstDonorPdfBytes)
      const secondDonorPdfDoc = await PDFDocument.load(secondDonorPdfBytes)
    
      const pdfDoc = await PDFDocument.create();
    
      const [firstDonorPage] = await pdfDoc.copyPages(firstDonorPdfDoc, [0])
      const [secondDonorPage] = await pdfDoc.copyPages(secondDonorPdfDoc, [0])
    
      pdfDoc.addPage(firstDonorPage)
      pdfDoc.insertPage(0,secondDonorPage)
    
      const pdfBytes = await pdfDoc.saveAsBase64({dataUri:true})
      let pdf = document.getElementById("create");
  
      pdf?.setAttribute('href',pdfBytes);
        this.monhttp.triggerMouse(pdf);
      
  }
  

  
}
