import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-conseiller',
  templateUrl: './conseiller.component.html',
  styleUrls: ['./conseiller.component.scss']
})
export class ConseillerComponent implements OnInit {


  constructor(private monhttp:HttpService) { }
  orders:any=[]
  
  pendingorders:any=[]
  okorders:any=[]
  cancelorders:any=[]
  tab:any=[];
  cpt=0
  crea_print_items:any=[]
  crea_okprint_items:any=[]
  crea_acpprint_items:any=[]
  crea_cancelprint_items:any=[]

  crea_pack_items:any=[]
  crea_okpack_items:any=[]
  crea_acppack_items:any=[]
  crea_cancelpack_items:any=[]

  crea_gadget_items:any=[]
  crea_okgadget_items:any=[]
  crea_acpgadget_items:any=[]
  crea_cancelgadget_items:any=[]

  crea_cloth_items:any=[]
  crea_cancelcloth_items:any=[]
  crea_okcloth_items:any=[]
  crea_acpcloth_items:any=[]

  crea_disp_items:any=[]
  crea_okdisp_items:any=[]
  crea_acpdisp_items:any=[]
  crea_canceldisp_items:any=[]

  editor_items:any=[]
  acpeditor_items:any=[]
  okeditor_items:any=[]
  canceleditor_items:any=[]
  ngOnInit(): void {
   
    this.monhttp.Aladin_orders().subscribe(
      res=>{
        this.orders=res.data
        console.log(this.orders)
        for(let item of this.orders){
          item.description= JSON.parse(item.description)
          if(item.description.type_product=="crea" && item.description.category=="vetement"){
            this.crea_cloth_items.push(item)
            console.log(this.crea_cloth_items)
           }
          if(item.description.type_product=="crea" && item.description.category=="emballage"){
            this.crea_pack_items.push(item)
          }
          if(item.description.type_product=="editor" ){
            this.editor_items.push(item)
          }
      if(item.description.type_product=="crea" && item.description.category=="imprimer"){
        item.crea_print_items.push(item)
      }
      //if(item.description.type_product=="crea" && item.description.category=="affichage"){
      //  item.crea_disp_items.push(item)
       // 
      //}
      if(item.description.type_product=="crea" && item.description.category=="gadget"){
        item.crea_gadget_items.push(item)
        console.log(item)
      }
    }
         
},
      err=>{
        console.log(err)
      }

    )
    this.monhttp.Getpending().subscribe(
      res=>{
        this.pendingorders=res.data
        console.log(res.data)
        for(let items of this.pendingorders){
          
          items.description= JSON.parse(items.description)
          if(items.description.type_product=="crea" && items.description.category=="vetement"){
           this.crea_acpcloth_items.push(items)
           
          }
          if(items.description.type_product=="crea" && items.description.category=="emballage"){
           this.crea_acppack_items.push()
           
          }
          if(items.description.type_product=="crea" && items.description.category=="imprimer"){
           this.crea_acpprint_items.push(items)
           
          }
          if(items.description.type_product=="crea" && items.description.category=="affichage"){
           this.crea_acpdisp_items.push(items)
           
          }
          if(items.description.type_product=="crea" && items.description.category=="gadget"){
           this.crea_acpgadget_items.push(items)
           
          }
         
         if(items.description.type_product=="editor" ){
           this.acpeditor_items.push(items)
         }
        
         console.log(this.crea_acpcloth_items)
          }
      },
      err=>{
        console.log(err)
      }

    )
  this.monhttp.GetordersLivre().subscribe(
    res=>{
      this.okorders=res.data
      console.log(this.okorders)
      for(let items of this.okorders){
        items.description= JSON.parse(items.description)
       if(items.description.type_product=="crea" && items.description.category=="vetement"){
        this.crea_okcloth_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="emballage"){
        this.crea_okpack_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="imprimer"){
        this.crea_okprint_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="affichage"){
        this.crea_okdisp_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="gadget"){
        this.crea_okgadget_items.push(items)
        
       }
      if(items.description.type_product=="editor" ){
        this.okeditor_items.push(items)
      }

    }
      
    }
    ,
      err=>{
        console.log(err)
      }
  )
  this.monhttp.getCancel().subscribe(
    res=>{
     this.cancelorders=res.data
     console.log(this.cancelorders)
    for(let items of this.cancelorders){
      items.description= JSON.parse(items.description)
     if(items.description.type_product=="crea" && items.description.category=="vetement"){
      this.crea_cancelcloth_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="emballage"){
      this.crea_cancelpack_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="imprimer"){
      this.crea_cancelprint_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="affichage"){
      this.crea_canceldisp_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="gadget"){
      this.crea_cancelgadget_items.push(items)
      
     }
    if(items.description.type_product=="editor" ){
      this.canceleditor_items.push(items)
    }
  }
   console.log(this.crea_cancelcloth_items)
    },
    err=>{
      console.log(err)
    }
  )
  }

updateStatus(event:any){
 let id = event.target.id
 if(id!=undefined){
  
   let data={id:id,status:'pending'}
   this.monhttp.Updatestatus(data).subscribe(
     res=>{
       console.log(res)
       if(res.status==true){
         this.monhttp.Getpending().subscribe(
           res=>{
             //console.log(res)
             this.pendingorders=res.data
             console.log( this.pendingorders)
           }
         )
       }
     },
     err=>{
       console.log(err)
     }
   )
 }
 
}
updateok(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'ok'}
    this.monhttp.updateok2(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.GetordersLivre().subscribe(
            res=>{
              //console.log(res)
              this.okorders=res.data
              console.log(this.okorders)
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}
updatecancel(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'rejected'}
    this.monhttp.Updatecancel(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.getCancel().subscribe(
            res=>{
              //console.log(res)
              this.cancelorders=res.data
              console.log(this.cancelorders)
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}



logout(){
  localStorage.removeItem('user');
  localStorage.removeItem('role');
  location.reload();
}
}
