import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CCGuard implements CanActivate {
  user_role=localStorage.getItem('role')
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if( this.user_role!=undefined&& this.user_role=="Conseiller clients"){
        return true
     }
     location.href="/login"
     return false
  }
  
}
